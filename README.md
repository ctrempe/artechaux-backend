# Hackathon 2023

Bienvenue dans le hackathon React de la LP MIAW 2023 !

Vous avez (environ) 30h pour créer un prototype d'application sur un sujet donné et préparer vos éléments de communication afin de *pitcher* votre projet lors d'une simulation de présentation devant un jury d'investisseurs.

## Contexte

Le milieu du bâtiment est resté relativement impénétrable (du moins pour le grand public) aux évolutions du numérique et du mobile.

Il y a pourtant un grand potentiel d'amélioration des outils existants, que ce soit en termes de fonctionnalités (mise en relation des artisans et des particuliers, de gestion de projet de chantier et maîtrise d'oeuvre, d'outil de communication autour d'un chantier entre différents artisans/clients, etc) ou d'ergonomie (les solutions existantes sont complexes à utiliser, ce qui rebute la plupart des artisans)...

## Sujet et déroulé

À l'aide du jeu de données fourni, vous devez imaginer un concept d'application web visant à révolutionner le secteur du bâtiment et en développer une démonstration (avec React bien sûr).

Votre présentation finale devra comporter au minimum un diaporama et une démo en direct de votre application.

Pour vous aider à avancer et ne pas rester bloqué, je serai à votre disposition tout au long de l'évènement pour vous aider.

Afin de tester votre concept et de vérifier que ce que vous avez imaginé correspond au besoin réel, un artisan du bâtiment viendra faire le tour des groupes pour discuter de votre projet avec vous en fin de journée du lundi. Il fera également partie du jury d'investisseurs lors de la présentation !

Ne perdez pas de vue votre objectif : vous devez séduire des investisseurs pour lancer lancer votre projet !

## Setup

Pour lancer le back-end fourni, entrez la commande suivante dans un terminal équipé de Docker :

```bash
mkdir -p ./directus_database
docker run -it \
    --name 2023_hackathon_directus \
    -v ./directus_uploads:/directus/uploads \
    -v ./directus_database:/directus/database \
    -p "8055:8055" \
    registry.gitlab.com/lp-miaw-react/2023-hackathon-backend:latest
```

## Directus

Afin que vous puissiez modifier les données facilement et éditer les structures de données pour les besoins de votre application, le jeu de données fourni dans une base SQLite est livré dans une image Docker qui embarque aussi un CMS headless nommé Directus.

Une fois l'image lancée, vous devriez pouvoir accéder à son interface à l'adresse suivante : [](http://localhost:8055/admin).
Les identifiants pour se connecter sont les suivants :

* Email : `admin@example.com`
* Mot de passe : `testpass`

Une API (REST et GraphQL) est également exposée par ce service.

N'hésitez pas à vous référer à la [documentation](https://docs.directus.io/) pour tous vos besoins.

## Les données

Le jeu de données fourni et inséré dans Directus contient les collections suivantes :

- `directus_users` : utilisateurs de votre application
- `directus_roles` : roles dans votre application
- `jobs` : différents métiers du domaine du bâtiment
- `craftsmen` : représente une entreprise d'artisans du bâtiment
- `projects` : des projets de travaux
- `services` : différentes tâches attachées à un projet
- `quotations` : devis d'un artisan pour les différentes prestations d'un projet
- `reviews` : avis concernant un artisan

